import sys
from reader_module.changer import PickleReader
from reader_module.changer import JsonReader
from reader_module.changer import CSVReader

input_file = sys.argv[1]
output_file = sys.argv[2]
change_requests = sys.argv[3:]

rd = PickleReader(input_file, output_file, change_requests)
rd.starter()