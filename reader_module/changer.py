import csv
import pathlib
import json
import pickle

class FileReaderBase:

    ALLOWED_EXTENSIONS = (
        'json', 
        'csv',
        'pickle'
    )

    def __init__(self, input_filename, output_filename, changes=[] ,input_path='', output_path=''):
        self.changes = changes
        self.change_requests = []
        self.input_filename = input_filename
        self.output_filename = output_filename
        self.input_path = input_path
        self.output_path = output_path
        self.filetype = self.set_filetype()
        self.output_filetype = self.set_output_filetype()
        self.validate = self.validate()
        self.data = self.set_data()

    def validate(self):
        if self.filetype not in self.ALLOWED_EXTENSIONS:
            print('Nieobslugiwany format')
            return False
        return True

    def set_filetype(self):
        return pathlib.Path(self.input_filename).suffix[1:]  

    def get_filepath(self):
        if self.input_path:
            return f'{self.input_path}/{self.input_filename}'
        return self.input_filename

    def set_data(self):
        with open(self.get_filepath(), self.read_bytes()) as file:
            if hasattr(self, f'get_{self.filetype}_data'):
                return getattr(self, f'get_{self.filetype}_data')(file)
            print(f'Konieczna implementacjametody: get_{self.filetype}_data.')
            return []

    def requested_changes(self):
        for request in self.changes:
            requests = request.split(',')
            self.change_requests.append(requests)

    def change_maker(self):
        for change in self.change_requests:
            row = int(change[0])
            column = int(change[1])
            value = change[2]
            self.data[row][column] = value        

    def set_output_filetype(self):
        return pathlib.Path(self.output_filename).suffix[1:]  

    def get_output_filepath(self):
        if self.output_path:
            return f'{self.output_path}/{self.output_filename}'
        return self.output_filename

    def save_data(self):
        with open (self.get_output_filepath(), self.write_bytes()) as file:
            if hasattr(self, f'save_{self.output_filetype}_data'):
                return getattr(self, f'save_{self.output_filetype}_data')(file)
            print(f'Konieczna implementacjametody: save_{self.output_filetype}_data.')
            return []

    def write_bytes(self):
        if self.filetype == 'pickle':
            return 'wb'
        else:
            return 'w'

    def read_bytes(self):
        if self.filetype == 'pickle':
            return 'rb'
        else:
            return 'r'

    def starter(self):
        self.read_bytes()
        self.write_bytes()
        self.requested_changes()
        self.change_maker()
        self.save_data()


class CSVReader(FileReaderBase):
    
    def get_csv_data(self, file): 
        data = []
        for line in file.readlines():
            data.append(line.replace('\n', '').split(','))
        return data

    def save_csv_data(self, file):
        thewriter = csv.writer(file)
        for item in self.data:
            thewriter.writerow(item)


class JsonReader(FileReaderBase):

    def get_json_data(self, file): 
        json_data = json.loads(file.read())
        return [[key, value] for key, value in json_data.items()]
    
    def save_json_data(self, file):
        json.dump(self.data, file, indent=4)


class PickleReader(FileReaderBase):

    def get_pickle_data(self, file):
        return pickle.load(file)
    
    def save_pickle_data(self, file):
        pickle.dump(self.data, file)